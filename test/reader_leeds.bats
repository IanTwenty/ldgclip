#!/usr/bin/bats

# SPDX-FileCopyrightText: 2022-4 The authors <https://gitlab.com/IanTwenty/ldgclip>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# Copy financial transactions from any website to your ledger journals with one
# click.
#
# For full copyright information see the AUTHORS file at the top-level
# directory of this distribution or at
# [AUTHORS](https://gitlab.com/IanTwenty/ldgclip)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

SCRIPT_PATH="$BATS_TEST_DIRNAME/../util/reader_leeds"

# Global
TEST_FILE=$(mktemp)

execute_reader() {
  # Function argument should be the transactions to test
  echo -e "$1" > "$TEST_FILE"

  run "$SCRIPT_PATH" "$TEST_FILE"

  echo "EXPECTING:"
  echo -e "$2"
  echo "GOT:"
  echo -e "$output"

  # Cleanup
  rm "$TEST_FILE"
}

function test_expectations() {
  # Pass expectations as first arg
  [ "$status" -eq 0 ]
  [ "$output" = "$1" ]
}

@test "reader_leeds: one transaction" {
  IFS='' test_input="01/01/2022	EXT TFR (WEB)	187.00		187.00"
  IFS='' test_exp="0001	2022/01/01	EXT TFR (WEB)	£-187.00"

  execute_reader "$test_input" "$test_exp"

  test_expectations "$test_exp"
}

@test "reader_leeds: two transactions" {
  IFS='' test_input="\
01/01/2022	EXT TFR (WEB)	13.00		167.00
31/12/2021	FASTER CR		180.00 	180.00"
  IFS='' test_exp="\
0001	2022/01/01	EXT TFR (WEB)	£-13.00
0002	2021/12/31	FASTER CR	£180.00"

  execute_reader "$test_input" "$test_exp"

  test_expectations "$test_exp"
}


