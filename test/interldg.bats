#!/usr/bin/env bats

# SPDX-FileCopyrightText: 2022-4 The authors <https://gitlab.com/IanTwenty/ldgclip>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# Copy financial transactions from any website to your ledger journals with one
# click.
#
# For full copyright information see the AUTHORS file at the top-level
# directory of this distribution or at
# [AUTHORS](https://gitlab.com/IanTwenty/ldgclip)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

@test "interldg: one transaction" {
  interfile=$(mktemp)
  cat >"$interfile" <<-EOF
  0001	2022/01/01	Description	£5
EOF

  run ./util/interldg -v code=CODE acc1=Assets acc2=Expenses <"$interfile"

  [ "$status" -eq 0 ]
  [ "${#lines[@]}" -eq 3 ]
  [ "${lines[0]}" = "2022/01/01 * (CODE) Description" ]
  [ "${lines[1]}" = "    Assets    £5" ]
  [ "${lines[2]}" = "    Expenses" ]
}

@test "interldg: one transaction, no code" {
  interfile=$(mktemp)
  cat >"$interfile" <<-EOF
  0001	2022/01/01	Description	£5
EOF

  run ./util/interldg -v acc1=Assets acc2=Expenses <"$interfile"

  [ "$status" -eq 0 ]
  [ "${#lines[@]}" -eq 3 ]
  [ "${lines[0]}" = "2022/01/01 * Description" ]
  [ "${lines[1]}" = "    Assets    £5" ]
  [ "${lines[2]}" = "    Expenses" ]
}
