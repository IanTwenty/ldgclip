#!/usr/bin/bats

# SPDX-FileCopyrightText: 2022-4 The authors <https://gitlab.com/IanTwenty/ldgclip>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# Copy financial transactions from any website to your ledger journals with one
# click.
#
# For full copyright information see the AUTHORS file at the top-level
# directory of this distribution or at
# [AUTHORS](https://gitlab.com/IanTwenty/ldgclip)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

SCRIPT_PATH="$BATS_TEST_DIRNAME/../util/reader_halifax"

# Global
TEST_FILE=$(mktemp)

execute_reader() {
  # Function argument should be the transactions to test
  echo -e "$1" >"$TEST_FILE"

  run "$SCRIPT_PATH" "$TEST_FILE"

  echo "EXPECTING:"
  echo -e "$2"
  echo "GOT:"
  echo -e "$output"

  # Cleanup
  rm "$TEST_FILE"
}

function test_expectations() {
  # Pass expectations as first arg
  [ "$status" -eq 0 ]
  [ "$output" = "$1" ]
}

@test "reader_halifax: one transaction" {
  IFS='' test_input="01 Jan 22	Stuff	99999999999999999	02 Jan 22	173.31"
  IFS='' test_exp="0001	2022/01/01=2022/01/02	Stuff	-£173.31"

  execute_reader "$test_input" "$test_exp"

  test_expectations "$test_exp"
}

@test "reader_halifax: two transactions" {
  IFS='' test_input="\
02 Sep 24 	CAR RENTAL 	99999999999999999 	03 Sep 24 	80.57
31 Aug 24 	AIR IMB EX 	99999999999999999 	02 Sep 24 	7.76"
  IFS='' test_exp="\
0001	2024/09/02=2024/09/03	CAR RENTAL	-£80.57
0002	2024/08/31=2024/09/02	AIR IMB EX	-£7.76"

  execute_reader "$test_input" "$test_exp"

  test_expectations "$test_exp"
}
