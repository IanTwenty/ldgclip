#!/usr/bin/env bats

# SPDX-FileCopyrightText: 2022-4 The authors <https://gitlab.com/IanTwenty/ldgclip>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# Copy financial transactions from any website to your ledger journals with one
# click.
#
# For full copyright information see the AUTHORS file at the top-level
# directory of this distribution or at
# [AUTHORS](https://gitlab.com/IanTwenty/ldgclip)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

setup_config() {
  # Callers must use LEDGERFILE in their config to have
  # it replaced by the temp filename we create here
  configfile=$(mktemp)
  ledgerfile=$(mktemp -u)
  ledgerfile_sed=${ledgerfile//\//\\/}
  cat | sed "s/LEDGERFILE/$ledgerfile_sed/g" >"$configfile"
}

clear_clipboard() {
  case "$XDG_SESSION_TYPE" in
  "x11")
    xclip -i /dev/null -l 1 || exit 1
    ;;
  "wayland")
    wl-copy --clear || exit 1
    ;;
  *)
    exit 1
    ;;
  esac
}

set_clipboard() {
  case "$XDG_SESSION_TYPE" in
  "x11")
    xclip
    ;;
  "wayland")
    # Paste once so wl-copy does not daemonize
    wl-copy --paste-once
    ;;
  *)
    exit 1
    ;;
  esac
}

close_clipboard() {
  # xclip process daemonizes itself as there must be a process holding
  # the selection if it is to be pasted later. We must kill this process when
  # we're finished or bats will wait forever for it to terminate.
  pkill xclip || true
  # wl-copy also does this but we use `--paste-once` above so it kills itself
}

# #############################################################################
# UNHAPPY PATHS
# #############################################################################

@test "ldgclip: config specified by LDGCLIP_CONFIG not found" {
  nonexistant_config="$(mktemp -u)"
  LDGCLIP_CONFIG="$nonexistant_config" run ./ldgclip

  [ $status -eq 1 ]
  [ "$output" = "ldgclip: config file $nonexistant_config is empty or does not exist" ]
}

@test "ldgclip: config specified by XDG_CONFIG_HOME not found" {
  nonexistant_config="$(mktemp -u)"
  XDG_CONFIG_HOME="$nonexistant_config" run ./ldgclip

  [ $status -eq 1 ]
  [ "$output" = "ldgclip: config file $nonexistant_config/ldgclip/config is empty or does not exist" ]
}

@test "ldgclip: empty clipboard does nothing" {
  setup_config <<-EOF
  Santander,LEDGERFILE,santander,@@@,@@@,
EOF
  clear_clipboard

  LDGCLIP_CONFIG="$configfile" LDGCLIP_CHOOSE=1 LDGCLIP_GUI=false ./ldgclip

  test ! -s "$ledgerfile"
}

@test "ldgclip: user aborts choice" {
  setup_config <<-EOF
  Santander,LEDGERFILE,santander,Assets,Expenses,MyCode
EOF

  LDGCLIP_CONFIG="$configfile" LDGCLIP_CHOOSE_ABORT=1 run ./ldgclip

  [ $status -eq 1 ]
  [ "$output" = "ldgclip: user aborted choice" ]
  test ! -s "$ledgerfile"
}

@test "ldgclip: user specifies non-existant reader" {
  setup_config <<-EOF
  Santander,LEDGERFILE,xxx,Assets,Expenses,MyCode
EOF
  IFS='' test_input="17/01/2020 	CARD PAYMENT TO SAINSBURYS PETROL,45.43 GBP, RATE 1.00/GBP ON 15-01-2020 		£45.43 	£54,084.23 "
  echo -e "$test_input" | set_clipboard

  LDGCLIP_CONFIG="$configfile" LDGCLIP_CHOOSE=1 LDGCLIP_GUI=false run ./ldgclip
  close_clipboard

  [ $status -eq 1 ]
  [ "$output" = "ldgclip: reader not found - xxx" ]
  test ! -s "$ledgerfile"
}

@test "ldgclip: user specifies non-existant journal" {
  nonexistant_path="$(mktemp -d -u)/nonexistant"
  setup_config <<-EOF
  Santander,$nonexistant_path,santander,Assets,Expenses,MyCode
EOF
  IFS='' test_input="17/01/2020 	CARD PAYMENT TO SAINSBURYS PETROL,45.43 GBP, RATE 1.00/GBP ON 15-01-2020 		£45.43 	£54,084.23 "
  echo -e "$test_input" | xclip

  LDGCLIP_CONFIG="$configfile" LDGCLIP_CHOOSE=1 LDGCLIP_GUI=false run ./ldgclip
  close_clipboard

  [ $status -eq 1 ]
  [ "$output" = "ldgclip: target journal's path does not exist! $nonexistant_path" ]
  test ! -s "$ledgerfile"
}

@test "ldgclip: user specifies journal path we cannot write to" {
  nonwritable_path=$(mktemp -p / -u)
  setup_config <<-EOF
  Santander,$nonwritable_path,santander,Assets,Expenses,MyCode
EOF
  IFS='' test_input="17/01/2020 	CARD PAYMENT TO SAINSBURYS PETROL,45.43 GBP, RATE 1.00/GBP ON 15-01-2020 		£45.43 	£54,084.23 "
  echo -e "$test_input" | xclip

  LDGCLIP_CONFIG="$configfile" LDGCLIP_CHOOSE=1 LDGCLIP_GUI=false run ./ldgclip
  close_clipboard

  [ $status -eq 1 ]
  [ "${lines[1]}" = "ldgclip: failed to append to journal! $nonwritable_path" ]
  test ! -s "$ledgerfile"
}

# #############################################################################
# HAPPY PATHS
# #############################################################################

@test "ldgclip: one transaction" {
  setup_config <<-EOF
  Santander,LEDGERFILE,santander,Assets,Expenses
EOF
  IFS='' test_input="17/01/2020 	CARD PAYMENT TO SAINSBURYS PETROL,45.43 GBP, RATE 1.00/GBP ON 15-01-2020 		£45.43 	£54,084.23 "
  echo -e "$test_input" | xclip

  LDGCLIP_CONFIG="$configfile" LDGCLIP_CHOOSE=1 ./ldgclip
  close_clipboard

  test -s "$ledgerfile"
  readarray -t results <"$ledgerfile"

  [ "${#results[@]}" -eq 4 ]
  [ "${results[0]}" = "" ]
  [ "${results[1]}" = "2020/01/17 * CARD PAYMENT TO SAINSBURYS PETROL,45.43 GBP, RATE 1.00/GBP ON 15-01-2020" ]
  [ "${results[2]}" = "    Assets    -£45.43" ]
  [ "${results[3]}" = "    Expenses" ]
}

@test "ldgclip: one transaction, unexpected working dir" {
  # This covers the case when we're launched by a GNOME shortcut
  # which sets working dir to $HOME
  setup_config <<-EOF
  Santander,LEDGERFILE,santander,Assets,Expenses
EOF
  IFS='' test_input="17/01/2020 	CARD PAYMENT TO SAINSBURYS PETROL,45.43 GBP, RATE 1.00/GBP ON 15-01-2020 		£45.43 	£54,084.23 "
  echo -e "$test_input" | xclip
  ldgclip_realpath="$(realpath ./ldgclip)"

  cd /tmp
  LDGCLIP_CONFIG="$configfile" LDGCLIP_CHOOSE=1 "$ldgclip_realpath"
  close_clipboard

  test -s "$ledgerfile"
  readarray -t results <"$ledgerfile"

  [ "${#results[@]}" -eq 4 ]
  [ "${results[0]}" = "" ]
  [ "${results[1]}" = "2020/01/17 * CARD PAYMENT TO SAINSBURYS PETROL,45.43 GBP, RATE 1.00/GBP ON 15-01-2020" ]
  [ "${results[2]}" = "    Assets    -£45.43" ]
  [ "${results[3]}" = "    Expenses" ]
}

@test "ldgclip: one transaction with code" {
  setup_config <<-EOF
  Santander,LEDGERFILE,santander,Assets,Expenses,MyCode
EOF
  IFS='' test_input="17/01/2020 	CARD PAYMENT TO SAINSBURYS PETROL,45.43 GBP, RATE 1.00/GBP ON 15-01-2020 		£45.43 	£54,084.23 "
  echo -e "$test_input" | xclip

  LDGCLIP_CONFIG="$configfile" LDGCLIP_CHOOSE=1 ./ldgclip
  close_clipboard

  test -s "$ledgerfile"
  readarray -t results <"$ledgerfile"

  [ "${#results[@]}" -eq 4 ]
  [ "${results[0]}" = "" ]
  [ "${results[1]}" = "2020/01/17 * (MyCode) CARD PAYMENT TO SAINSBURYS PETROL,45.43 GBP, RATE 1.00/GBP ON 15-01-2020" ]
  [ "${results[2]}" = "    Assets    -£45.43" ]
  [ "${results[3]}" = "    Expenses" ]
}

@test "ldgclip: one transaction with filter" {
  setup_config <<-EOF
  Santander,LEDGERFILE,santander,Assets,Expenses,,sed s/CARD/CREDIT/g
EOF
  IFS='' test_input="17/01/2020 	CARD PAYMENT TO SAINSBURYS PETROL,45.43 GBP, RATE 1.00/GBP ON 15-01-2020 		£45.43 	£54,084.23 "
  echo -e "$test_input" | xclip

  LDGCLIP_CONFIG="$configfile" LDGCLIP_CHOOSE=1 ./ldgclip
  close_clipboard

  test -s "$ledgerfile"
  readarray -t results <"$ledgerfile"

  [ "${#results[@]}" -eq 4 ]
  [ "${results[0]}" = "" ]
  [ "${results[1]}" = "2020/01/17 * CREDIT PAYMENT TO SAINSBURYS PETROL,45.43 GBP, RATE 1.00/GBP ON 15-01-2020" ]
  [ "${results[2]}" = "    Assets    -£45.43" ]
  [ "${results[3]}" = "    Expenses" ]
}
