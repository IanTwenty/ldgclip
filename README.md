<!--
SPDX-FileCopyrightText: 2022-4 The authors <https://gitlab.com/IanTwenty/ldgclip>

SPDX-License-Identifier: CC-BY-SA-4.0

Copy financial transactions from any website to your ledger journals with one
click.

For full copyright information see the AUTHORS file at the top-level
directory of this distribution or at
[AUTHORS](https://gitlab.com/IanTwenty/ldgclip)

This work is licensed under the Creative Commons Attribution 4.0 International
License. You should have received a copy of the license along with this work.
If not, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to
Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
-->

# ldgclip

![A clipboard holds a piece of paper with 'ldgclip' written on it and a small pile of coins](ldgclip.png)

Copy financial transactions from any website to your
[ledger](https://www.ledger-cli.org/) journals with one click.

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Roadmap](#roadmap)
- [Contributing](#contributing)
- [License](#license)

## Background

Converting transactions from websites to the ledger format is a pain. Each site
presents the data in different non-compatible formats or requires you to
download transaction files taking several mouse clicks.

Ldgclip makes the process easy - simply copy the transactions you see in the
browser to the clipboard and run ldgclip. It'll pop up a window and ask you to
choose a target journal. Once selected the transactions in the clipboard will be
converted and appended to your journal. Through configuration you can choose
what accounts and codes are applied to the new transactions automatically.

It can also pass transactions through other ledger import tools and is
specifically designed to work with
[ldgmark](https://gitlab.com/IanTwenty/ldgmark)

## Install

We only support Linux right now, more may be supported in future on request.
You can git clone this repo and run `ldgclip` straight from the repo dir.

In order to access the clipboard we require either [xclip](https://repology.org/project/xclip/versions)
(for X server) or [wl-clipboard](https://repology.org/project/wl-clipboard/versions)
(for Wayland) to be installed.

## Usage

On first usage you need to configure ldgclip so it knows what to do when
summoned or nothing will happen. Ldgclip expects a config file at
`$XDG_CONFIG_HOME/ldgclip/config` where each line is a comma-separated set of
fields specifying a target journal:

```csv
NICKNAME,JOURNAL,READER,1ST ACCOUNT NAME,2ND ACCOUNT NAME,[OPTIONAL CODE],[OPTIONAL FILTER]
```

For example we configure two target journals:

```csv
Halifax,~/halifax.ldg,halifax,Assets:Savings,Expenses:Purchases,Halifax
Virgin,~/virgin.ldg,virgin,Assets:Savings,Expenses:Purchases,Virgin Money
```

If we look at this config as a table it will be clearer:

| Nickname | Journal       | Reader  | 1st Account Name | 2nd Account Name   | Code         | Filter |
| -------- | ------------- | ------- | ---------------- | ------------------ | ------------ | ------ |
| Halifax  | ~/halifax.ldg | halifax | Assets:Savings   | Expenses:Purchases | Halifax      |        |
| Virgin   | ~/virgin.ldg  | virgin  | Assets:Savings   | Expenses:Purchases | Virgin Money |        |

Each field needs further explanation:

- **Nickname**: A nickname by which to refer to this target, for your own
  reference. This will be used in the dialog that pops up to help you select a
  target.
- **Journal**: the path of the journal file that'll receive the transactions
  from the clipboard (they are appended).
- **Reader**: this is used to choose a 'reader' that interprets the data on the
  clipboard and converts it to ledger transactions. This is where you tell
  ldgclip what institution you have copied your transactions from. We only have
  a few right now but please let us know if your website is missing and we'll add
  it:
  - `family`: [Family Building Society](https://www.familybuildingsociety.co.uk/)
  - `halifax`: [Halifax](https://www.halifax-online.co.uk/)
  - `leeds`: [Leeds Building Society](http://www.leedsbuildingsociety.co.uk/index.html)
  - `santander` : [Santander](https://www.santander.co.uk/)
  - `transact` : [Transact](https://www.transact-online.co.uk/)
  - `usbank`: [U.S. Bank](https://www.usbank.com/index.html)
- **1st Account Name**: the account name to use for the first posting on each transaction.
- **2nd Account Name**: the account name to use for the second posting on each transaction.
- **Code**: an (optional) textual "code" for the transaction. See Ledger's docs on
  [Codes](https://www.ledger-cli.org/3.0/doc/ledger3.html#Codes) for more info.
  If you don't use Codes then you don't need to specify this.
- **Filter**: an (optional) BASH cmd through which to pass the transactions
  before they are finally appended to the journal. This could be another ledger
  import tool for example [ldgmark](https://gitlab.com/IanTwenty/ldgmark). The
  cmd should expect the transactions on stdin and produce the filtered result on
  stdout. This is perfect for adding additional metadata, marking transactions
  etc.

Now we have a config file we can use ldgclip. To continue the example imagine we
visit the Halifax website and copy the transactions we want into the clipboard.
Simply run `ldgclip` and this dialog pops up:

![A dialog from ldgclip asks the user to select a destination ledger file,
virgin or halifax.](Screenshot.png)

This is prompting us to choose a destination journal. Select `halifax.ldg` and
you'll find the transactions in your clipboard added to the end of the
`~/halifax.ldg` journal in ledger format:

```ledger
...

2022/01/01 * (Halifax) Stewart Lee - Content Provider
    Assets:Savings                       £8
    Expenses:Purchases
```

To make it easier to run ldgclip you could:

- Setup a global hotkey to run `ldgclip`. In GNOME use
  Settings-Keyboard-Customise Shortcuts-Custom Shortcuts.
- Trigger ldgclip automatically with a clipboard manager such as
  [hluk/CopyQ](https://github.com/hluk/CopyQ).

There are a couple of cmdline flags available:

```bash
Usage:
  ldgclip [option]

Flags:
  -d, --debug            print debug information during run
  -h, --help             help for ldgclip
```

There are also some environment variables that can control `ldgclip`:

- `LDGCLIP_CONFIG` - specify a path to an alternative config
- `LDGCLIP_CHOOSE` - choose the given ledger file without prompting. This is
  1-based so the first ledger file in the config is 1, 2nd is 2 etc.
- `LDGCLIP_CHOOSE_ABORT` - abort the choice of ledger file without prompting,
  this is only useful for testing. If `LDGCLIP_CHOOSE` is also present it takes
  precendence.
- `LDGCLIP_GUI` - set to false to get error messages on stdout instead of zenity
  dialogs. Only useful for testing.

## Roadmap

To do:

- Use devenv pre-commit hooks for our linting and testing
- Better title for the dialog "Choose journal"
- Expand banks available.

Done:

- 10th Nov 2024
  - Use devenv.sh for dev environment.
  - Added Wayland support via use of [bugaevc/wl-clipboard](https://github.com/bugaevc/wl-clipboard)
- 5th Sept 2024
  - Fix minor bug in Halifax reader.
  - Add Skipton bank.
- 3rd May 2022
  - The order of journals in the dialog should match that in the config file.
- 26th Apr 2022
  - Run filters on transactions before appending to ledger file
- 25th Apr 2022
  - Simplify configuration, use CSV format
  - Abort if config file is missing/empty
  - It's hard to select a file, paths are often too long to fit in the default
    dialog size. Instead show a custom name first, details after.
  - Show errors when we cannot write to a journal
  - Allow use of `~` in config file
- 21st Apr 2022
  - Added Leeds bank.
  - Added Family bank.
  - We should fail if we can't find our reader.
  - User can abort ledger file selection.
  - Run properly when we're not in the expected working dir. This
    also fixes case when we're run by GNOME shortcut.
  - Add a debug flag for troubleshooting
- 20th Apr 2022
  - Actually use accounts and codes when writing transactions.
  - Proper build and licensing

## Contributing

This software is young and probably has lots of problems and gaps in the
documentation! Please help us by submitting any
[issues](https://gitlab.com/IanTwenty/ldgclip/-/issues) or questions that occur
to you. Also see `CONTRIBUTING.md`.

## License

We declare our licensing by following the [REUSE
specification](https://reuse.software/) - copies of applicable licenses are
stored in the `LICENSES` directory. Here is a summary:

- All source code is licensed under [GPL-3.0-or-later](https://spdx.org/licenses/GPL-3.0-or-later.html).
- If it's not executable, including the text when extracted from code, it is
  licensed under [CC-BY-SA-4.0](https://spdx.org/licenses/CC-BY-SA-4.0.html).

For more accurate information, check individual files.

ldgclip is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.
